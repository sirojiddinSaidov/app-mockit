package uz.pdp.appmockito.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import uz.pdp.appmockito.collection.User;

import java.util.Optional;

public interface UserRepo extends MongoRepository<User, String> {
    Optional<User> findByUsername(String username);

}
