package uz.pdp.appmockito.payload;


public record UserDTO(String username, String password, String name) {
}
