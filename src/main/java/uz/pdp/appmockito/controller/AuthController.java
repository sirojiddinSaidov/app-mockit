package uz.pdp.appmockito.controller;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.appmockito.collection.User;
import uz.pdp.appmockito.payload.UserDTO;
import uz.pdp.appmockito.service.UserService;

@RestController
@RequestMapping("/api/auth")
//@RequiredArgsConstructor
public class AuthController {


    private UserService userService;

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public HttpEntity<User> register(@RequestBody UserDTO userDTO) {
        User user = userService.signUp(userDTO);
        return ResponseEntity.status(201).body(user);
    }

}
