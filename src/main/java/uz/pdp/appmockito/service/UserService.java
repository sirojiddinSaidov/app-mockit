package uz.pdp.appmockito.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.pdp.appmockito.collection.User;
import uz.pdp.appmockito.exceptions.RestException;
import uz.pdp.appmockito.payload.UserDTO;
import uz.pdp.appmockito.repository.UserRepo;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepo userRepo;


    public User signUp(UserDTO userDTO) {
        if (userDTO == null)
            throw new RestException("Oka null");
        Optional<User> optionalUser = userRepo.findByUsername(userDTO.username());
        if (optionalUser.isPresent())
            throw new RestException("User already exists");
        User user = User.builder()
                .name(userDTO.name())
                .password(userDTO.password())
                .username(userDTO.username())
                .build();
        return userRepo.save(user);
    }
}
