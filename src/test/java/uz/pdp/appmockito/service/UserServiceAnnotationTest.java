package uz.pdp.appmockito.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import uz.pdp.appmockito.collection.User;
import uz.pdp.appmockito.exceptions.RestException;
import uz.pdp.appmockito.payload.UserDTO;
import uz.pdp.appmockito.repository.UserRepo;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceAnnotationTest {

    private UserService userService;

    @Mock
    private UserRepo userRepo;

    @BeforeEach
    void setUp() {
        userService = new UserService(userRepo);
    }

    @Test
    void signUpSuccessTest() {


        UserDTO userDTO = new UserDTO("ketmon", "123", "Uroqo");

        User extpected = new User(
                "sajdkljasld",
                userDTO.username(),
                userDTO.password(),
                userDTO.name());

        when(userRepo.save(any())).thenReturn(extpected);

        User actual = userService.signUp(userDTO);
        assertEquals(extpected.getUsername(), actual.getUsername());
    }


    @Test
    void signUpParameterNullFailTest() {
        assertThrows(
                RestException.class,
                () -> userService.signUp(null));
    }

    @Test
    void signUpAlreadyExistsFailTest() {

        when(userRepo.findByUsername(anyString())).thenReturn(Optional.of(new User()));

        assertThrows(RestException.class,
                () -> userService.signUp(new UserDTO("bla", "battar", "asd")));

    }
}