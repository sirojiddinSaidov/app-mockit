package uz.pdp.appmockito.controller;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.http.HttpEntity;
import uz.pdp.appmockito.collection.User;
import uz.pdp.appmockito.service.UserService;

import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class AuthControllerTest {


    @Test
    void registerSuccessTest() throws NoSuchFieldException, IllegalAccessException {
        AuthController authController = new AuthController();

        UserService userService = Mockito.mock(UserService.class);

        authController.setUserService(userService);

        when(userService.signUp(any())).thenReturn(null);

        HttpEntity<User> result = authController.register(null);
        User body = result.getBody();
        assertNull(body);
    }
}